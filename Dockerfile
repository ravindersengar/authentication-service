FROM openjdk:8-jdk-alpine
EXPOSE 4001
VOLUME /main-app
ADD /target/authentication-service-0.1.0.jar authentication-service-0.1.0.jar
ENTRYPOINT ["java","-jar","authentication-service-0.1.0.jar"]