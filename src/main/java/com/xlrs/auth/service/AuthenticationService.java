package com.xlrs.auth.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.xlrs.auth.entity.AuthUser;
import com.xlrs.auth.repository.AuthUserRepository;
import com.xlrs.auth.view.AuthUserView;
import com.xlrs.commons.util.EncryptionUtil;

@Service
public class AuthenticationService implements UserDetailsService {

    @Autowired
    private AuthUserRepository authUserRepository;
    
    @Autowired
	private MessageSource messages;
    
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AuthUser user = authUserRepository.findByUserName(username);
        if (user == null) {
            throw new UsernameNotFoundException(messages.getMessage("err.invalid.credentials", null, null));
        } else {
            return create(user);
        }
    }
    
    public UserDetails getUserById(Long id) throws UsernameNotFoundException {
        Optional<AuthUser> user = authUserRepository.findById(id);
        if (user == null || user.get() == null) {
            throw new UsernameNotFoundException(messages.getMessage("error.user.not.found", null, null));
        } else {
            return create(user.get());
        }
    }
    
    public AuthUserView authenticateUser(String username, String password, Long orgId) throws Exception {
        AuthUser user = authUserRepository.findByUserNameAndPasswordAndOrganisationId(username,  EncryptionUtil.encrypt(password), orgId);
        AuthUserView userVO = new AuthUserView();
        if (user == null) {
        	throw new UsernameNotFoundException(messages.getMessage("error.user.not.found", null, null));
        } else {
        	BeanUtils.copyProperties(user, userVO);
        	userVO.setUsername(user.getUserName());
        }
        return userVO;
    }
    
	private AuthUserView create(com.xlrs.auth.entity.AuthUser user) {
		return new AuthUserView(user.getId(), user.getUserName(), user.getName(), user.getEmail(), user.getPassword(),
				user.getUserRoles(), user.getOrganisationId());
	}
}
