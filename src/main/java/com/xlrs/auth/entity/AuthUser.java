package com.xlrs.auth.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.xlrs.commons.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "organisation_user")
@Getter
@Setter
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class AuthUser extends AbstractEntity {

	private static final long serialVersionUID = 1L;

    @Size(min = 4, max = 50)
    private String userName;
    
    @Size(min = 4, max = 50)
    private String name;

    @Size(min = 4, max = 100)
    private String password;

    @Size(min = 4, max = 20)
    private String userRoles;
    
	private String email;
	
	private Long organisationId;
	
}