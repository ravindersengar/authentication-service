package com.xlrs.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xlrs.auth.entity.AuthUser;

public interface AuthUserRepository extends JpaRepository<AuthUser, Long> {
    
	AuthUser findByUserName(String username);

	AuthUser findByUserNameAndPasswordAndOrganisationId(String username, String encrypt, Long orgId);
}
