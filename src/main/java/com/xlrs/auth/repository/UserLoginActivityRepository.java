package com.xlrs.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xlrs.auth.entity.UserLoginActivity;

@Repository
public interface UserLoginActivityRepository extends JpaRepository<UserLoginActivity, Integer> {
	
}
